import numpy as np

def euler_integration(delta_t, n, mode):
	s = [1]
	if mode == "explicit":
		for i in range(1, n+1):
			result = s[i-1] * (1 - 10 * delta_t)
			s.append(result)
		return round(s[n], 6)
	elif mode == "implicit":
		for i in range(1, n+1):
			result = s[i-1] / (1 + 10 * delta_t)
			# result = s[i-1] - 10 * delta_t * result
			s.append(result)
		return round(s[n], 6)

if __name__ == "__main__":
	n = 4
	delta_t = [0.05, 0.1, 0.2, 0.25]
	explicit_results = []
	implicit_results = []
	for dt in delta_t:
		explicit_results.append(euler_integration(dt, n, 'explicit'))
		implicit_results.append(euler_integration(dt, n, 'implicit'))

	print("Explicit Results: ", explicit_results)
	print("Implicit Results: ", implicit_results)

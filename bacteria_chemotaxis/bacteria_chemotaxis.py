import random


def behavior(rho, m_i, d_i, p_i, p_d):
	""" Defines the behavior of bacteria.

			Parameters:
			rho: Concentration of the nutrient.
			m_i: last concentration of the nutrient.
			d_i: last direction taken (N, S, E, W).
			p_i: probability of tumbling when concentration increases.
			p_d: probability of tumbling when concentration decreases.
	"""

	if rho <= m_i:
		p = p_d
	else:
		p = p_i

	if random.random() <= p:
		return rho, randomDirection()
	else:
		return rho, d_i


def randomDirection():
	return random.choice(['N', 'S', 'E', 'W'])


rho = 0.7
m_i = 0.6
d_i = 'S'
p_i = 0.4
p_d = 0.6
print(behavior(rho, m_i, d_i, p_i, p_d))
